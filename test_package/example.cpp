#include <cstdlib>
#include <iostream>

#include "magic_enum.hpp"

enum Color { RED = 2, BLUE = 4, GREEN = 8 };
int main(int argc, const char** argv) {
    (void)argc, (void)argv;

    Color color = Color::RED;
    auto color_name = magic_enum::enum_name(color);
    return 0;
}
