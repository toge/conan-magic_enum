from conans import ConanFile, CMake, tools

class CppTaskflowConan(ConanFile):
    name           = "magic_enum"
    version        = "0.6.0"
    license        = "MIT"
    url            = "https://bitbucket.org/toge/conan-magic_enum/"
    description    = "Static reflection for enums (to string, from string, iteration) for modern C++, work with any enum type without any macro or boilerplate code "
    no_copy_source = True

    def source(self):
        tools.download("https://github.com/Neargye/magic_enum/releases/download/v{}/magic_enum.hpp".format(self.version), "magic_enum.hpp")

    def package(self):
        self.copy("*.hpp", src=".", dst="include", keep_path=True)

    def package_info(self):
        self.info.header_only()
